<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'name' => 'toto',
            'email' => 'toto@toto.com',
            'password' => bcrypt('toto'),
            'user_role' => 'Admin',
        ]);

        //factory(App\Don::class, 20)->create();
    }
}
