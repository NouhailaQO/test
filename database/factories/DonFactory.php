<?php

use Faker\Generator as Faker;

$factory->define(App\Don::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'desc' => $faker->text,
        'minMoney' => $faker->numberBetween(1, 300)
    ];
});
