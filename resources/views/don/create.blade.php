@extends('layouts.donLayout',[
'homeVar' =>'',
'donVar' => 'active',
'faqVar' =>''
])

@section('stylesheets')
    @parent
    <style>
        .btn-circle {
            border-radius: 50%;
        }
    </style>
@endsection

@section('content')
    <div class="clean-block">
        <div class="container">
            <div class="block-heading">
                <h1>Create new don</h1>
            </div>

            <div class="row justify-content-center">
                <form action="/dons" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control form-control-lg" id="title" name="title"
                               aria-describedby="titleHelp"
                               placeholder="Enter title" required>
                        <small id="titleHelp" class="form-text text-muted">Attract people with title.</small>
                    </div>
                    <div class="form-group">
                        <label for="desc">Description</label>
                        <textarea class="form-control form-control-lg" id="desc" name="desc"
                                  placeholder="Description" required>
                            </textarea>
                    </div>
                    <div class="form-group">
                        <label for="minMoney">Money Start</label>
                        <div class="input-group">
                            <input type="number" min="0" class="form-control form-control-lg" id="minMoney"
                                   name="minMoney" placeholder="Start Money $ $ $" required>
                            <div class="input-group-append">
                                <div class="input-group-text">DT</div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-dark">Don</button>
                </form>
            </div>

@endsection