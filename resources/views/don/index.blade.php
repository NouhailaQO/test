@extends('layouts.donLayout',[
'homeVar' =>'',
'donVar' => 'active',
'faqVar' =>''
])

@section('stylesheets')
    @parent
    <style>
        .btn-circle {
            border-radius: 50%;
        }

        /*a .icon {
            color: white !important;
        }*/

        .col-md-4{
            margin-bottom: 20px;
        }
    </style>
@endsection

@section('content')
    <div class="clean-block">
        <div class="container">
            <div class="block-heading">
                <img src="img/DonationIcon.png" width="30%">
                <p>List of Dons</p>
            </div>
            {{-- dump($dons) --}}
            <div class="row justify-content-center">
                <div class="row mb-3 d-block text-right" style="width: 80%">
                    <a class="btn btn-success" href="{{ route('newDon') }}">Add</a>
                </div>
                @foreach( $dons as $don )
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{ $don->title }}</h5>
                            <p class="card-text">{{ $don->desc }}</p>
                            <a class="btn btn-danger btn-circle text-right"><i class="ion-ios-heart icon"></i></a>
                            start : {{ $don->minMoney }} DT
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row justify-content-center">
                {{ $dons->links() }}
            </div>

        </div>


    </div>
@endsection