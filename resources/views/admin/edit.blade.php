@extends('layouts.app')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <div class="row container">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Don</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('home') }}"> Back</a>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <form action="{{ route('home.update',$don->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control form-control-lg" id="title" name="title"
                       aria-describedby="titleHelp"
                       placeholder="Enter title" value="{{ $don->title }}" required>
                <small id="titleHelp" class="form-text text-muted">Attract people with title.</small>
            </div>
            <div class="form-group">
                <label for="desc">Description</label>
                <textarea class="form-control form-control-lg" id="desc" name="desc"
                          placeholder="Description" value="{{ $don->desc }}" required>
                            </textarea>
            </div>
            <div class="form-group">
                <label for="minMoney">Money Start</label>
                <div class="input-group">
                    <input type="number" min="0" class="form-control form-control-lg" id="minMoney"
                           name="minMoney" placeholder="Start Money $ $ $" value="{{ $don->minMoney }}" required>
                    <div class="input-group-append">
                        <div class="input-group-text">DT</div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>

        </form>
    </div>


@endsection