@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <table class="table table-bordered">
                <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Start Money</th>
                    <th>User</th>
                    <th width="280px">Action</th>
                </tr>
                @foreach ($dons as $don)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>{{ $don->title }}</td>
                        <td>{{ $don->desc }}</td>
                        <td>{{ $don->minMoney }}</td>

                        @if ($don->user)
                            <td>{{ $don->user->name }}</td>
                        @else
                            <td></td>
                        @endif

                        <td>
                            <form action="{{ route('home.destroy',$don->id) }}" method="POST">
                                <a class="btn btn-primary" href="{{ route('home.edit',$don->id) }}">Edit</a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>

            {!! $dons->links() !!}

            {{--
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>


                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        You are logged in!
                    </div>
                </div>
            </div>
            --}}
        </div>
    </div>
@endsection
