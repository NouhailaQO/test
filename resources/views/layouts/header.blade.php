<nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
    <div class="container"><a class="navbar-brand logo" href="#">Don ISSATSo</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span
                    class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse"
             id="navcol-1">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item" role="presentation"><a class="nav-link {{ homeVar }}" href="/">Home</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link {{ donVar }} "
                                                            href="{{ route('listDons') }}">DON</a></li>
                @auth
                    {{--@if (auth()->user()->isAdministrator())
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/admin">Dashboard</a></li>
                    @else
                        <li class="nav-item" role="presentation"><a class="nav-link {{ faqVar }}" href="/faq">FAQ</a>
                        </li>
                    @endif--}}
                    <li class="nav-item" role="presentation"><a class="nav-link" href="/admin">Dashboard</a></li>

                @elseguest
                    <li class="nav-item" role="presentation"><a class="nav-link {{ faqVar }}" href="/faq">FAQ</a></li>

                @endauth
            </ul>
        </div>
    </div>
</nav>