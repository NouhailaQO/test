<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

@section('stylesheets')

    <!-- Styles -->
        <link href="{{ mix('css/app.css') }}" rel="stylesheet"/>
        <link href="{{ mix('css/webStyle.css') }}" rel="stylesheet"/>
    @show
</head>
<body>
<nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
    <div class="container"><a class="navbar-brand logo" href="/">Don ISSATSo</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span
                    class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse"
             id="navcol-1">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item" role="presentation"><a class="nav-link {{ $homeVar }}" href="/">Home</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link {{ $donVar }} "
                                                            href="{{ route('listDons') }}">DON</a></li>
                @auth
                    {{--@if (auth()->user()->isAdministrator())
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/admin">Dashboard</a></li>
                    @else
                        <li class="nav-item" role="presentation"><a class="nav-link {{ $faqVar }}" href="/faq">FAQ</a>
                        </li>
                    @endif--}}
                    <li class="nav-item" role="presentation"><a class="nav-link" href="/admin">Dashboard</a></li>

                @elseguest
                    <li class="nav-item" role="presentation"><a class="nav-link {{ $faqVar }}" href="/faq">FAQ</a></li>

                @endauth
            </ul>
        </div>
    </div>
</nav>
<main class="page landing-page" id="app">
    @yield('content')
</main>

<div class="footer-basic">
    <footer>
        <div class="social"><a href="#"><i class="icon ion-logo-instagram"></i></a><a href="#"><i
                        class="icon ion-logo-snapchat"></i></a><a href="#"><i class="icon ion-logo-twitter"></i></a><a
                    href="#"><i class="icon ion-logo-facebook"></i></a></div>
        <ul class="list-inline">
            <li class="list-inline-item"><a href="/">Home</a></li>
            <li class="list-inline-item"><a href="{{ route('listDons') }}">Don</a></li>
            <li class="list-inline-item"><a href="#">About</a></li>
        </ul>
        <p class="copyright">ISSATSo © 2018</p>
    </footer>
</div>

@section('javascripts')
    <script type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
@show

</body>
</html>