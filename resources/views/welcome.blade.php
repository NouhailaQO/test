<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Don ISSATSo</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet"/>
    <link href="{{ mix('css/webStyle.css') }}" rel="stylesheet"/>
    <style>
        {{--html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }--}}
    </style>

</head>
<body>
{{--<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            Laravel
        </div>

        <div class="links">
            <a href="https://laravel.com/docs">Documentation</a>
            <a href="https://laracasts.com">Laracasts</a>
            <a href="https://laravel-news.com">News</a>
            <a href="https://nova.laravel.com">Nova</a>
            <a href="https://forge.laravel.com">Forge</a>
            <a href="https://github.com/laravel/laravel">GitHub</a>
        </div>
    </div>
</div>--}}

<nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
    <div class="container"><a class="navbar-brand logo" href="/">Don ISSATSo</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span
                    class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse"
             id="navcol-1">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item" role="presentation"><a class="nav-link active" href="/">Home</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link" href="{{ route('listDons') }}">DON</a></li>
                @auth
                    {{--@if (auth()->user()->isAdministrator())
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/admin">Dashboard</a></li>
                    @else
                        <li class="nav-item" role="presentation"><a class="nav-link" href="/faq">FAQ</a>
                        </li>
                    @endif--}}
                    <li class="nav-item" role="presentation"><a class="nav-link" href="/admin">Dashboard</a></li>
                @elseguest
                    <li class="nav-item" role="presentation"><a class="nav-link" href="/faq">FAQ</a></li>

                @endauth
            </ul>
        </div>
    </div>
</nav>
<main class="page landing-page" id="app">
    <section class="clean-block clean-hero"
             style="background-image:url(&quot;img/dobBack.jpg&quot;);color:rgba(92,184,92,0.8);">
        <div class="text">
            <h2>ISSATSo donation.
                <div class="caption v-middle text-center">
                    <h1 class="cd-headline clip">
                        <span class="blc">DON | </span>
                        <span class="cd-words-wrapper">
			              <b class="is-visible">ISSATSo.</b>
			              <b>Sousse.</b>
			              <b>Tunisia.</b>
			            </span>
                    </h1>
                </div>
            </h2>
            <p></p>
            <a class="btn btn-outline-light btn-lg" href="{{ route('listDons') }}">DON</a>
        </div>
    </section>

    <section class="clean-block features">
        <div class="container">
            <div class="block-heading">
                <h2 class="text-info" style="color: #5cb85c !important;">Features</h2>
                <p>.</p>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-5 feature-box"><i class="icon-arrow-up icon"></i>
                    <h4>Advantage Someone</h4>
                    <p>.</p>
                </div>
                <div class="col-md-5 feature-box"><i class="icon-pencil icon"></i>
                    <h4>Save</h4>
                    <p>.</p>
                </div>
                <div class="col-md-5 feature-box"><i class="icon-shuffle icon"></i>
                    <h4>Evolve</h4>
                    <p>.</p>
                </div>
                <div class="col-md-5 feature-box"><i class="icon-reload icon"></i>
                    <h4>Did the correct thing</h4>
                    <p>.</p>
                </div>
            </div>
        </div>
    </section>

</main>

<div class="footer-basic">
    <footer>
        <div class="social"><a href="#"><i class="icon ion-logo-instagram"></i></a><a href="#"><i
                        class="icon ion-logo-snapchat"></i></a><a href="#"><i class="icon ion-logo-twitter"></i></a><a
                    href="#"><i class="icon ion-logo-facebook"></i></a></div>
        <ul class="list-inline">
            <li class="list-inline-item"><a href="/">Home</a></li>
            <li class="list-inline-item"><a href="{{ route('listDons') }}">Don</a></li>
            <li class="list-inline-item"><a href="#">About</a></li>
        </ul>
        <p class="copyright">ISSATSo © 2018</p>
    </footer>
</div>

<!--Scripts-->
<script type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
<style>
    .feature-box .icon {
        color: #5cb85c;
    }
</style>
</body>
</html>
