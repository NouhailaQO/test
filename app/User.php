<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function dons()
    {
        return $this->hasMany('App\Don');
    }

    public function delete()
    {
        DB::transaction(function () {
            $this->dons()->delete();
            parent::delete();
        });
    }

    public function isAdministrator()
    {
        return $this->getAttributeValue('user_role') == 'Admin';
    }

    public function isSimpleUser()
    {
        return $this->getAttributeValue('user_role') == 'User';
    }

}
