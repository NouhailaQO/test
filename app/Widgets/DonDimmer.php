<?php
/**
 * Created by PhpStorm.
 * User: MacbookProDeAY
 * Date: 18/12/2018
 * Time: 14:13
 */

namespace App\Widgets;

use App\Don;
use TCG\Voyager\Widgets\BaseDimmer;
use Illuminate\Support\Str;

class DonDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Don::count();
        $string = "Dons";

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-group',
            'title'  => "{$count} {$string}",
            'text'   => "You have {$count} dons in your database. Click on button below to view all dons.",
            'button' => [
                'text' => "View all dons",
                'link' => route('voyager.dons.index'),
            ],
            'image' => voyager_asset('images/widget-backgrounds/01.jpg'),
        ]));
    }

}