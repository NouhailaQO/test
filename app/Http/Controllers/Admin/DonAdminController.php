<?php
/**
 * Created by PhpStorm.
 * User: MacbookProDeAY
 * Date: 19/12/2018
 * Time: 12:32
 */

namespace App\Http\Controllers\Admin;

use App\Don;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Http\Controllers\VoyagerBaseController as BaseType;

class DonAdminController extends BaseType
{
    public function create(Request $request)
    {
        if (Auth::user()->hasRole('user')) {
            //var_dump(Auth::user());
        }
        return parent::create($request);
    }

}