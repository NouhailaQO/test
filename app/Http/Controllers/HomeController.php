<?php

namespace App\Http\Controllers;

use App\Don;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dons = Don::paginate(9);
        return view('home')->with('dons', $dons)->with('i', (request()->input('page', 1) - 1) * 9);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Don $don
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.edit')->with('don', Don::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Don $don
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'desc' => 'required',
            'minMoney' => 'required',
        ]);

        $don = Don::findOrFail($id);
        $don->update($request->all());

        return redirect()->route('home')
            ->with('status', 'Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Don $don
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Don::destroy($id))
            $msg = "Don deleted successfully";
        else
            $msg = "Don not deleted";

        return redirect()->route('home')->with('status', $msg);
    }

}
