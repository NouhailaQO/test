<?php

namespace App\Http\Controllers;

use App\Don;
use Illuminate\Http\Request;

class DonController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $dons = Don::paginate(9);
        return view('don.index')->with('dons', $dons);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('don.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $don = new Don;
        $don->title = trim($request->title);
        $don->desc = trim($request->desc);
        $don->minMoney = $request->minMoney;
        $don->user_id = auth()->user()->id;
        $don->save();
        //Don::create(['title' => trim($request->title), 'desc' => trim($request->desc), 'minMoney' => $request->minMoney]);
        return redirect()->route('listDons');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Don $don
     * @return \Illuminate\Http\Response
     */
    public function show(Don $don)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Don $don
     * @return \Illuminate\Http\Response
     */
    public function edit(Don $don)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Don $don
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Don $don)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Don $don
     * @return \Illuminate\Http\Response
     */
    public function destroy(Don $don)
    {
        //
    }
}
